<?php

namespace App\Services;

use Denpa\Bitcoin\Client;
use GuzzleHttp\Client as GuzzleHttp;

class BitcoinClient extends Client {
    /**
     * Construct Bitcoin client
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        // Config
        $this->config = [
            'scheme'        => 'http',
            'host'          => env('BTCNODE_HOST'),
            'port'          => env('BTCNODE_PORT'),
            'user'          => env('BTCNODE_USERNAME'),
            'password'      => env('BTCNODE_PASSWORD'),
            'ca'            => null,
            'preserve_case' => false,
        ];

        // Construct client
        $this->client = new GuzzleHttp([
            'base_uri' => $this->getDsn(),
            'auth'     => $this->getAuth(),
            'verify'   => $this->getCa(),
            'handler'  => $this->getHandler(),
            'proxy' => env('PROXY')
        ]);
    }
}