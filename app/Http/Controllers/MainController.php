<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BitcoinClient;

class MainController extends Controller
{
    public function index()
    {
        return response()->json([
            'credentials' => [
                'address' => env('BTCNODE_HOST').':'.env('BTCNODE_PORT'),
                'username' => env('BTCNODE_USERNAME'),
                'password' => env('BTCNODE_PASSWORD'),
            ],
            'balance' => BitcoinClient::request('getbalance')->getContainer()['result'],
            'addresses' => BitcoinClient::request('listreceivedbyaddress')->getContainer()['result'],
            'transactions' => BitcoinClient::request('listtransactions')->getContainer()['result']
        ], 200);
    }

    /**
     * Balance
     */
    public function balance()
    {
        return response()->json([
            'balance' => BitcoinClient::request('getbalance')->getContainer()['result']
        ], 200);
    }

    /**
     * Addresses
     */
    public function addresses()
    {
        return response()->json([
            'addresses' => BitcoinClient::request('listreceivedbyaddress')->getContainer()['result']
        ], 200);
    }

    /**
     * Create address
     */
    public function create_address()
    {
        $new_address = BitcoinClient::request('getnewaddress')->getContainer()['result'];

        return response()->json([
            'address' => $new_address,
            'balance' => 0
        ], 200);
    }
}
