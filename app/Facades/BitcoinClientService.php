<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class BitcoinClientService extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'BitcoinClient';
    }
}