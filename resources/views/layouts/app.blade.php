<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <title>Polygant Test</title>
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>

        <!-- JavaScript -->
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>